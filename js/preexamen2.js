var capital = document.getElementById('capital');
var lenguaje = document.getElementById('lenguaje');
function mostrardatos(data) {
for (let item of data) {
    capital.innerHTML = item.capital;
    if (item.languages) {
        let languagesList = Object.keys(item.languages).map(key => item.languages[key]);
        lenguaje.innerHTML = languagesList;
    } else {
        lenguaje.innerHTML = "No se encontraron idiomas";
    }
}
console.log(data);
}


document.getElementById('search').addEventListener('click', ()=>{
var name = document.getElementById('countryname').value;
if(isNaN(name)){
const url = 'https://restcountries.com/v3.1/name/'+name;
fetch(url)
.then(response => response.json())
.then(data => mostrardatos(data))
.catch((reject)=>{
    console.log("surgio un error"+reject);
    alert("No existe");
});
}else{
    alert("Incorrecto");
}

});

document.getElementById('clean').addEventListener('click', ()=>{
    capital.innerHTML="";
    lenguaje.innerHTML="";
})
