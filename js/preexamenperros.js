mostrardatos = (data) => {
    var selection = document.getElementById('breed');
    for(let item of data.message){
        var option = document.createElement("option");   
        option.innerHTML=item;
        selection.append(option);
    }
}

cargarImagen = (breed) => {
    axios.get('https://dog.ceo/api/breed/' + breed + '/images/random')
    .then(response =>{
        var img = document.createElement('img');
        img.src = response.data.message;
        var imgContainer = document.getElementById('contenimg');
        imgContainer.innerHTML = '';
        imgContainer.appendChild(img);
    })
    .catch(error => {
        console.error('Error fetching data:', error);
        alert("No se pudo cargar la imagen");
    });
}

document.getElementById('loadbreed').addEventListener('click', function(){
    axios.get('https://dog.ceo/api/breeds/list')
    .then(response =>{
        mostrardatos(response.data);
    })
    .catch(error => {
        console.error('Error fetching data:', error);
        alert("No se pudo cargar la lista de razas");
    });
})

document.getElementById('breed').addEventListener('change', function(){
    var selectedBreed = this.value;
    if(selectedBreed){
        cargarImagen(selectedBreed);
    }
});

document.getElementById('seeimage').addEventListener('click', function(){
    cargarImagen('hound');
});