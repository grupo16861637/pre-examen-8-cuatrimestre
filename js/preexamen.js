mostrardatos = (data) => {
    document.getElementById('name').value=data.name;
    document.getElementById('username').value=data.username;
    document.getElementById('email').value=data.email;
    document.getElementById('street').value=data.address.street;
    document.getElementById('number').value=data.address.zipcode;
    document.getElementById('city').value=data.address.city;
    
}

document.getElementById('show').addEventListener('click', () => {    
var id = parseInt(document.getElementById('id').value);
if(isNaN(id)){
    alert("Ingresa Correctamente")
}else{
    axios.get('https://jsonplaceholder.typicode.com/users/'+id)
    .then(response =>{
     mostrardatos(response.data);
    })
    .catch(error => {
      console.error('Error fetching data:', error);
      alert("No existe ese numero")
    });
}

});
